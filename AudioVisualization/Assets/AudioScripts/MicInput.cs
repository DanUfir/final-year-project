﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MicInput : MonoBehaviour {

    string mic;
    AudioSource audio;
    float sensitivity = 100f;
    public float loudness = 0f;
    public float freq;
    public int sampleRate = 11024;
   
    public float[] samples = new float[512];

    public float[] freqBands = new float[8];
    float[] sampleData = new float[8192];

    public float avgAmplitude = 0;

    public float Freq
    {
        get
        {
            return freq;
        }

        set
        {
            freq = value;
        }
    }
    
    // Use this for initialization
    void Start()
    {
        audio = GetComponent<AudioSource>();
        foreach (string device in Microphone.devices)
        {
            if (mic == null)
            {
                mic = device; // make default device equal to the mic
            }
        }
        UpdateMic();

    
    }

    // Update is called once per frame
    void Update()
    {
        loudness = GetAvgVolume() * sensitivity;
        Freq = FundamentalFreq();

        audio.GetSpectrumData(sampleData, 0, FFTWindow.Hamming);
        freqBands = ampBands();
    }
     

    void UpdateMic()
    {
        audio.Stop();

        audio.clip = Microphone.Start(mic, true, 10, sampleRate);
        audio.loop = true;

        if (Microphone.IsRecording(mic))
        {
            while (!(Microphone.GetPosition(mic) > 0))
            { }

            audio.Play();
        }
        else
        {
            Debug.Log("Mic not working");
        }
    }
    public float GetAvgVolume()
    {
        float[] data = new float[256];
        float average = 0;
        audio.GetOutputData(data, 0);
        foreach (float k in data)
        {
            average += Mathf.Abs(k);
        }
        return average / 256;

    }

    #region
    /*______________________________________
     Title: Fundamental frequencies and detecting notes
     Author: TeppoK
     Site owner/sponsor:
     Date: Jan 3 2013
     Code version: 
     Availability:  http://www.kaappine.fi/tutorials/fundamental-frequencies-and-detecting-notes/ 
     (Accessed: 01 March 2017)
     Modified: The following code was refactored and modified in order to get fundemental frequencies as well as average amplitude values
    -------------------------------------------------------------------*/

    public float FundamentalFreq()
    {
        //Initilize the fundemental frequency as 0
        float funFreq = 0f;
        //Create empty array to store the data
        float[] sampleData = new float[8192];
        //Populate the data using the spectrum method and applying the Hamming window
        audio.GetSpectrumData(sampleData, 0, FFTWindow.Hamming);
        //Initilize the strongest frequency signal as 0
        float strongstSignal = 0f;
        //Get the index of the strongest signal
        int index = 0;
        //Initilize the amplitude values as 0
        float ampValues = 0f;
        //Loop through all the data
        for (int k = 1; k < 8192; k++)
        {
            //Add the amplitude values (own code)
            ampValues += sampleData[k];
            //Check if current value stronger than frequency 
            if (strongstSignal < sampleData[k])
            {
                //Set the strongest signal to the current value
                strongstSignal = sampleData[k];
                //Set the index
                index = k;
            }
        }
        //Caluclate sample amplitude (own code)
        avgAmplitude = ampValues * sampleRate / 8124;
        //Calculate sample frequency
        funFreq = index * sampleRate / 8124;

        return funFreq;
    }
    #endregion

    /*
    Own code modified from getting the frequency bands
    */
    public float [] ampBands()
    {
        float[] ampBands = new float[8];
        float ampValues = 0;
        float[] samplesB = new float[512];
        audio.GetSpectrumData(samplesB, 0, FFTWindow.Hamming);
        int count = 0;        
        for (int i = 0; i < 8; i++)
        {
            int samplesCount = (int)Mathf.Pow(2, i) * 2;
            if (i == 7)
            {
                samplesCount += 2;
            }
            for (int k = 0; k < samplesCount; k++)
            {
                ampValues += samplesB[count] * (count + 1);
                count++;
            }
            ampBands[i] = ampValues / count;            
        }
        return ampBands;
    }
}
