﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmplitudeFrequency : MonoBehaviour {
    float frequency;
    VectorN vec;

    public AmplitudeFrequency(float freq, VectorN vector)
    {
        frequency = freq;
        vec = vector;
    }

    public float Frequency
    {
        get
        {
            return frequency;
        }

        set
        {
            frequency = value;
        }
    }

    public VectorN Vec
    {
        get
        {
            return vec;
        }

        set
        {
            vec = value;
        }
    }

    public AmplitudeFrequency getAverage(List<AmplitudeFrequency> list)
    {
        VectorN averageVecN = new VectorN(list[0].vec.dim);
        float averageFreq = 0f;

        for(int i = 0; i < list.Count; i ++)
        {
            averageVecN += list[i].vec;
            averageFreq += list[i].frequency;
        }

        averageVecN /= list.Count;
        averageFreq /= list.Count;

        AmplitudeFrequency averageAmpFreq = new AmplitudeFrequency(averageFreq, averageVecN);
        return averageAmpFreq;
    }
    
    public static float Distance(AmplitudeFrequency a, AmplitudeFrequency b)
    {
        float distAmp =  VectorN.Distance(a.vec, b.vec);
        float distFreq = a.frequency - b.frequency;

        float distance = distAmp + distFreq;
        return distance;
        
    }
}
