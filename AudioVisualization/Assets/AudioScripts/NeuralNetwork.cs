﻿using UnityEngine;
using System.Collections;

public class NeuralNetwork : MonoBehaviour {

    //Inputting the data
    private int noOfInputs;
    private int noOfOutputs;
    private int noOfHiddenLayer;

    //storing the data original data
    private double[] inputs;
    private double[][] weights;
    private double[] outputs;
    private double[] bias;

    //data after itteration
    private double[][] iWeights;
    private double[] iOutputs;
    private double[] iBias;

    private double errorValues;
    //used for assigning initial random weight 
    private Random random;

    public int NoOfInputs
    {
        get
        {
            return noOfInputs;
        }

        set
        {
            noOfInputs = value;
        }
    }

    public int NoOfOutputs
    {
        get
        {
            return noOfOutputs;
        }

        set
        {
            noOfOutputs = value;
        }
    }

    public int NoOfHiddenLayer
    {
        get
        {
            return noOfHiddenLayer;
        }

        set
        {
            noOfHiddenLayer = value;
        }
    }

    public double[] Inputs
    {
        get
        {
            return inputs;
        }

        set
        {
            inputs = value;
        }
    }

    public double[][] Weights
    {
        get
        {
            return weights;
        }

        set
        {
            weights = value;
        }
    }

    public double[] Outputs
    {
        get
        {
            return outputs;
        }

        set
        {
            outputs = value;
        }
    }

    public double[] Bias
    {
        get
        {
            return bias;
        }

        set
        {
            bias = value;
        }
    }

    public double[][] IWeights
    {
        get
        {
            return iWeights;
        }

        set
        {
            iWeights = value;
        }
    }

    public double[] IOutputs
    {
        get
        {
            return iOutputs;
        }

        set
        {
            iOutputs = value;
        }
    }

    public double[] IBias
    {
        get
        {
            return iBias;
        }

        set
        {
            iBias = value;
        }
    }

    public Random Random
    {
        get
        {
            return random;
        }

        set
        {
            random = value;
        }
    }

    public double ErrorValues
    {
        get
        {
            return errorValues;
        }

        set
        {
            errorValues = value;
        }
    }

    public NeuralNetwork(int noOfInputs, int noOfOutputs, int noOfHiddenLayer)
    {
        this.NoOfInputs = noOfInputs;
        this.NoOfOutputs = noOfOutputs;
        this.NoOfHiddenLayer = noOfHiddenLayer;
    }

    public double TrainNetwork(double[][] data, int noIterations, double learnRate, double momentum)
    {
        return 0f;
    }

    void adjustWeights(double[][] weights, double[][] iWeights)
    {
        for (int i = 0; i < weights.Length; i++)
        {
            for(int j = 0; j < weights[i].Length; j++)
            {
               // iWeights = 
            }
        }

    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
