﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (AudioSource))]
public class AudioInput : MonoBehaviour {

    AudioSource audio;
    public FFTWindow window;
    float sensitivity = 100f;
    public float loudness = 0f;
    public float freq = 0f;
    public int sampleRate = 11024;

    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        //    GetSpectrumSource();
        loudness = GetAvgVolume() * sensitivity;
        freq = FundamentalFreq();

    }

    void GetSpectrumSource()
    {
        audio.GetSpectrumData(sampleRate, 0, window);
    }
    float GetAvgVolume()
    {
        float[] data = new float[256];
        float average = 0;
        base.GetComponent<AudioSource>().GetOutputData(data, 0);
        foreach (float k in data)
        {
            average += Mathf.Abs(k);
        }
        return average / 256;

    }
    #region
    /*______________________________________
     Title: Fundamental frequencies and detecting notes
     Author: TeppoK
     Site owner/sponsor:
     Date: Jan 3 2013
     Code version: 
     Availability:  http://www.kaappine.fi/tutorials/fundamental-frequencies-and-detecting-notes/ 
     (Accessed: 01 March 2017)
     Modified: The following code was refactored and modified in order to get fundemental frequencies
    -------------------------------------------------------------------*/

    float FundamentalFreq()
    {
        float funFreq = 0f;
        float[] sampleData = new float[8192];
        base.GetComponent<AudioSource>().GetSpectrumData(sampleData, 0, FFTWindow.Hamming);
        float strongstSignal = 0f;
        int index = 0;

        for (int k = 1; k < 8192; k++)
        {
            if (strongstSignal < sampleData[k])
            {
                strongstSignal = sampleData[k];
                index = k;
            }
        }
        funFreq = index * sampleRate / 8124;

        return funFreq;
    }
    #endregion
}
