﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class VectorN
{
    //Size of the list
    internal int dim;
    //List of amplitude vales
    List<float> element = new List<float>();

    //Constructor for list value input
    public VectorN(List<float> values)
    {
        dim = values.Count;
        element = values;
    }

    //Constructor for float array input
    public VectorN(float[] values)
    {
        element.Clear();

        dim = values.Length;
        foreach (float f in values)
        {
            element.Add(f);           
        }
       
    }

    //Constructor for size input
    public VectorN(int dimension)
    {
        dim = dimension;
        element = new List<float>();

        for (int i = 0; i < dim; i++)
        {
            element.Add(0);
        }
    }

    //Getting the average values for the list
    public VectorN getAverage(List<VectorN> list)
    {
        VectorN average = new VectorN(list[0].dim);

        for (int i = 1; i < list.Count; i++)
        {
            average = average + list[i];
        }

        return average / list.Count;
    }

    //Getting the Distance between two vectors
    public static float Distance(VectorN a, VectorN b)
    {
        float dist = 0;

        for (int i = 0; i < a.dim; i++)
        {
            dist += Mathf.Pow((a.element[i] - b.element[i]),2);
        }
        return Mathf.Sqrt(dist);
    }

    //Adding two VectorN
    public static VectorN operator + (VectorN a, VectorN b)
    {
        float[] list = new float[a.dim];
        for (int i = 0; i < a.dim; i++)
        {
            list[i] = a.element[i] + b.element[i];           
        }
        VectorN result = new VectorN(list);

        return result;
    }

    //Dividing VectorN by a float value
    public static VectorN operator / (VectorN a, float b)
    {
        VectorN result = new VectorN(a.dim);

        for (int i = 0; i < a.dim; i++)
        {
            result.element[i] = a.element[i] / b;
        }
          
        return result;
    }


    //Displaying the VectorN
    public string ToString()
    {
        string ans = "";
    
        for(int i =0; i < dim; i++)
        {
            ans += element[i] + "\n";
        }
        return ans;
    }
}

