﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecPos : MonoBehaviour {
    //Instance of the microphone class
    MicInput mic;
    VectorN vec;
    float curFreq = 0;

    public static List<VectorN> rightVal = new List<VectorN>(150);
    public static List<VectorN> upVal = new List<VectorN>(150);
    public static List<VectorN> downVal = new List<VectorN>(150);
    public static List<VectorN> leftVal = new List<VectorN>(150);

    public static List<float> rightValFreq = new List<float>(150);
    public static List<float> upValFreq = new List<float>(150);
    public static List<float> downValFreq = new List<float>(150);
    public static List<float> leftValFreq = new List<float>(150);

    int counterA = 0;
    int counterB = 0;
    int counterC = 0;
    int counterD = 0;

    bool checkA = false;
    bool checkB = false;
    bool checkC = false;
    bool checkD = false;
    // Use this for initialization
    void Start () {
        mic = GetComponent<MicInput>();
	}
	
	// Update is called once per frame
	void Update () {
        vec = new VectorN(mic.ampBands());
        curFreq = mic.FundamentalFreq();

        //Key Inputs
        #region
        if (Input.GetKeyDown(KeyCode.A))
        {
            checkA = true;      
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            checkD = true;
        }
      
        if (Input.GetKeyDown(KeyCode.W))
        {
            checkC = true;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            checkB = true;
        }
        if (checkA == true)
        {
            if (counterA < 160)
            {
                recLeft();
                counterA++;
            }
        }
        if (checkB == true)
        {
            if (counterB < 160)
            {
                recRight();
                counterB++;
            }
        }
    
        if(checkC == true)
        {
            if (counterC < 160)
            {
                recUp();
                counterC++;
            }
        }
       
        if(checkD==true)
        {
            if (counterD < 160)
            {
                recDown();
                counterD++;
            }
        }
        #endregion
    }

    void recLeft()
    {
        if(leftVal.Count < 150)
        {
            leftVal.Add(vec);
            leftValFreq.Add(curFreq);
        }
        else
        {
            leftVal.RemoveAt(0);
            leftVal.Add(vec);
        }
    }

    void recRight()
    {
        if (rightVal.Count < 150)
        {
             rightVal.Add(vec);
            rightValFreq.Add(curFreq);
            
        }
        else
        {
            rightVal.RemoveAt(0);
            rightVal.Add(vec);
        }
    }
    
    void recUp()
    {
        if (upVal.Count < 150)
        {            
            upVal.Add(vec);
            upValFreq.Add(curFreq);
        }
        else
        {
            upVal.RemoveAt(0);
            upVal.Add(vec);
        }
    }

    void recDown()
    {
        if (downVal.Count < 150)
        {            
            downVal.Add(vec);
            downValFreq.Add(curFreq);
        }
        else
        {
            downVal.RemoveAt(0);
            downVal.Add(vec);
        }
    }
    
}
