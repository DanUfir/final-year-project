﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorAnalysis : MonoBehaviour {

    MicInput mic;
    RecPos rec;
        
    List<AmplitudeFrequency> list = new List<AmplitudeFrequency>();
    List<VectorN> ls = new List<VectorN>();

    VectorN nothing = new VectorN(8);

    List<float> frequencies = new List<float>();
    
    bool updated = false;
    private AmplitudeFrequency current;
    private AmplitudeFrequency empty;
    private AmplitudeFrequency leftPosition;
    private AmplitudeFrequency downPosition;
    private AmplitudeFrequency upPosition;
    private AmplitudeFrequency rightPosition;

    // public VectorN curValue;
    // Use this for initialization
    void Start () {
        mic = GetComponent<MicInput>();
        rec = GetComponent<RecPos>();
        empty = new AmplitudeFrequency(0, nothing);
    }
	
	// Update is called once per frame
	void Update () {
        VectorN value = currentValues();
        float freq = mic.FundamentalFreq();

        current = new AmplitudeFrequency(freq, value);

        if (Input.GetKeyDown(KeyCode.M))
        {
            updated = true;       
        }
    
        if (updated == true)
        {
            UpdatePositions();
        }
        
        int index = 5;

        if(list.Count > 0)
        {
            float closestDistance = 1000;
            index = 5;
            for (int i = 0; i < list.Count; i++)
            {
                if (mic.GetAvgVolume() < 0.01f)
                {
                    index = 5;
                }
                else
                {
                   float distanceBetween = Mathf.Abs(AmplitudeFrequency.Distance(current, list[i]));
                if (distanceBetween < closestDistance)
                    {
                        index = i;
                        closestDistance = distanceBetween;
                    }
                }                
            }
        }
        
        if (index == 0)
        {
           MoveLeft();            
        }
        else if(index == 1)
        {
            MoveRight();
        }
        else if(index == 2)
        {
            MoveUp();
        }
        else if(index == 3)
        {
            MoveDown();
        }
        else
        {
            DoNothing();
        }
    }

   
    void UpdatePositions()
    {      
        leftPosition = new AmplitudeFrequency(getAverageFreq(RecPos.leftValFreq), getAverage(RecPos.leftVal));
        rightPosition = new AmplitudeFrequency(getAverageFreq(RecPos.rightValFreq), getAverage(RecPos.rightVal));
        upPosition = new AmplitudeFrequency(getAverageFreq(RecPos.upValFreq), getAverage(RecPos.upVal));
        downPosition = new AmplitudeFrequency(getAverageFreq(RecPos.downValFreq), getAverage(RecPos.downVal));

        list.Add(leftPosition);
        list.Add(rightPosition);
        list.Add(upPosition);
        list.Add(downPosition);

        updated = false;
        

    }
    //Movement
    #region
    void MoveLeft()
    {
        Debug.Log("left");
        transform.position += new Vector3(-1, 0, 0);
        
    }
    void MoveRight()
    {
        Debug.Log("right");
        transform.position += new Vector3(1, 0, 0);
    }

    void MoveUp()
    {
        Debug.Log("up");
        transform.position += new Vector3(0, 1, 0);
    }
    void MoveDown()
    {
        Debug.Log("down");
        transform.position += new Vector3(0, -1, 0);
    }
    void DoNothing()
    {

    }
    #endregion
    VectorN currentValues()
    {
        float[] array = mic.ampBands();
        VectorN vec = new VectorN(array);
        return vec;
    }

 
    VectorN getAverage(List<VectorN> list)
    {
        VectorN average = new VectorN(list[0].dim);
        for (int i = 1; i < list.Count; i++)
        {
            average = average + list[i];

        }
        return average / list.Count;
    }
    
    float getAverageFreq(List<float> list)
    {
        float avg = 0;
        for(int i = 0; i < list.Count; i++)
        {
            avg += list[i];
        }
        return avg /= list.Count;
    }


}
