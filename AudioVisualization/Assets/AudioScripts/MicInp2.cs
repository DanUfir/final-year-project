﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicInp2 : MonoBehaviour {

    public float averageAmp = 0;
    public float[] freqBands = new float[8];
    float[] samples = new float[512];
    float[] freqBand = new float[8];
    float[] freqBand64 = new float[64];
    AudioSource audio;
    string mic;
    public int sampleRate = 11024;

    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
        foreach (string device in Microphone.devices)
        {
            if (mic == null)
            {
                mic = device; // make default device equal to the mic
            }
        }
        UpdateMic();
    }
	
	// Update is called once per frame
	void Update () {
        getSpectrumAudioSource();
        MakeFrequencyBands();
	}
    void UpdateMic()
    {
        audio.Stop();

        audio.clip = Microphone.Start(mic, true, 10, sampleRate);
        audio.loop = true;

        if (Microphone.IsRecording(mic))
        {
            while (!(Microphone.GetPosition(mic) > 0))
            { }

            audio.Play();
        }
        else
        {
            Debug.Log("Mic not working");
        }
    }
    void getSpectrumAudioSource()
    {
        audio.GetSpectrumData(samples, 0, FFTWindow.BlackmanHarris);
    }
    #region
    /*______________________________________
     Title: AudioPeer
     Author: Peter Olthof
     Site owner/sponsor:
     Date: Sept 20 2016
     Code version: 
     Availability:  https://www.youtube.com/watch?v=mHk3ZiKNH48&t=611s
     (Accessed: 01 March 2017)
     Modified: The following code was refactored and modified in order to get amplitude values  
    -------------------------------------------------------------------*/
    
    void MakeFrequencyBands()
    {
        int count = 0;
        for (int i = 0; i < 8; i++)
        {
            averageAmp = 0;
            int samplesCount = (int)Mathf.Pow(2, i) * 2;
            if (i == 7)
            {
                samplesCount += 2;
            }
            for (int k = 0; k < samplesCount; k++)
            {
                averageAmp += samples[count] * (count + 1);
                count++;
            }
            averageAmp /= count;
            freqBands[i] = averageAmp * 10;
        }
    }
    #endregion

}
