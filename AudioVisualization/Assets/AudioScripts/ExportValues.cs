﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ExportValues : MonoBehaviour {

    public float timer;
    bool write = false;
    MicInput mic;
    System.IO.StreamWriter file;

    public GameObject cube;
    //public GameObject[] cubeStorage;
    List<Vector3> positions = new List<Vector3>();
    List<float> heightValues = new List<float>();

    public Material m;
    bool done = false;

    // Use this for initialization
    void Start() {
        mic = GetComponent<MicInput>();
        file = new System.IO.StreamWriter(@"C:\Users\Dan\Desktop\test\test.txt");
        timer = 0f;
     
    }

    // Update is called once per frame
    void Update() {
        ExportToText();
        timer += Time.deltaTime;
    }

    //Storing the data in a text file
    void ExportToText()
    {
        float[] array = mic.ampBands();

        StringBuilder data = new StringBuilder();
        for (int i = 0; i < array.Length; i++)
        {
            data.Append(array[i]).Append(" ");
            heightValues.Add(array[i]);
        }

        if (timer <= 10f)
        {
            file.WriteLine(data);
        }

        else
        {
            file.Close();
            CreateGraph();
        }

    }
    //Method to create the graph of the values
    void CreateGraph()
    {            
        if(done == false)
        {
            for (int a = 0; a < heightValues.Count / 8; a++)
            {
                for (int l = 0; l < 8; l++)
                {
                    Vector3 pos = new Vector3(l * 1.5f, 0, a * 1.25f);
                    //Debug.Log(pos);
                    positions.Add(pos);
                }
            }

            int count = 0;
            int num = 0;
            //loop through the position and create appropriate cubes with the amplitude values
            for (int i = 0; i < positions.Count; i++)
            {
                GameObject cubeInstance = Instantiate(cube);               

                cubeInstance.transform.position = positions[i];
                cubeInstance.transform.localScale = new Vector3(1, heightValues[i] * 10000, 1);
                
                if(num == 0)
                {
                    cubeInstance.GetComponent<Renderer>().material.color = Color.red;
                }
                else
                {
                    cubeInstance.GetComponent<Renderer>().material.color = Color.black;
                }

                count++;
                if(count == 8)
                {
                    count = 0;
                    num++;
                    if(num == 2)
                    {
                        num = 0;
                    }
                }
            }
            done = true;
        }
       
    } 

    void ChangeColor(GameObject cube)
    {
        Color color = new Color(Random.Range(0, 255), Random.Range(0, 255), Random.Range(0, 255));
        cube.GetComponent<Renderer>().material.color = color;
        Debug.Log(color);
      //  return cube;
    }
}
