﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoundExtraction
{
    class WavHead
    {
        int chunkId;
        int chunkSize;
        int chunkFormat;
        int subchunk1Id;
        int subchunk1Size;
        int audioFormat;
        int numChannels;
        int sampleRate;
        int byteRate;
        int blockAllign;
        int bitsPerSample;
        int subchunk2Id;
        int subchank2Size;
        byte [] data;

        public WavHead()
        {

        }
        public WavHead(int chunkId, int chunkSize, int chunkFormat, int subchunk1Id, int subchunk1Size, int audioFormat, int numChannels, int sampleRate, int byteRate, int blockAllign, int bitsPerSample, int subchunk2Id, int subchank2Size, byte[] data)
        {
            this.chunkId = chunkId;
            this.chunkSize = chunkSize;
            this.chunkFormat = chunkFormat;
            this.subchunk1Id = subchunk1Id;
            this.subchunk1Size = subchunk1Size;
            this.audioFormat = audioFormat;
            this.numChannels = numChannels;
            this.sampleRate = sampleRate;
            this.byteRate = byteRate;
            this.blockAllign = blockAllign;
            this.bitsPerSample = bitsPerSample;
            this.subchunk2Id = subchunk2Id;
            this.subchank2Size = subchank2Size;
            this.data = data;
        }

        public int ChunkId
        {
            get
            {
                return chunkId;
            }

            set
            {
                chunkId = value;
            }
        }

        public int ChunkSize
        {
            get
            {
                return chunkSize;
            }

            set
            {
                chunkSize = value;
            }
        }

        public int ChunkFormat
        {
            get
            {
                return chunkFormat;
            }

            set
            {
                chunkFormat = value;
            }
        }

        public int Subchunk1Id
        {
            get
            {
                return subchunk1Id;
            }

            set
            {
                subchunk1Id = value;
            }
        }

        public int Subchunk1Size
        {
            get
            {
                return subchunk1Size;
            }

            set
            {
                subchunk1Size = value;
            }
        }

        public int AudioFormat
        {
            get
            {
                return audioFormat;
            }

            set
            {
                audioFormat = value;
            }
        }

        public int NumChannels
        {
            get
            {
                return numChannels;
            }

            set
            {
                numChannels = value;
            }
        }

        public int SampleRate
        {
            get
            {
                return sampleRate;
            }

            set
            {
                sampleRate = value;
            }
        }

        public int ByteRate
        {
            get
            {
                return byteRate;
            }

            set
            {
                byteRate = value;
            }
        }

        public int BlockAllign
        {
            get
            {
                return blockAllign;
            }

            set
            {
                blockAllign = value;
            }
        }

        public int BitsPerSample
        {
            get
            {
                return bitsPerSample;
            }

            set
            {
                bitsPerSample = value;
            }
        }

        public int Subchunk2Id
        {
            get
            {
                return subchunk2Id;
            }

            set
            {
                subchunk2Id = value;
            }
        }

        public int Subchank2Size
        {
            get
            {
                return subchank2Size;
            }

            set
            {
                subchank2Size = value;
            }
        }

        public byte[] Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }
    }
}
