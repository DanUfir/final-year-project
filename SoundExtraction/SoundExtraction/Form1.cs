﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio.Dsp;

namespace SoundExtraction
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void loadFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
           

            //CustomWaveViewer.WaveStream = new NAudio.Wave.WaveFileReader(open.FileName);
            //CustomWaveViewer.FitToScreen();

            chart1.Series.Add("wave");
                        chart1.Series["wave"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
                        chart1.Series["wave"].ChartArea = "ChartArea1";

            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Wave File (*.wav)|*.wav;";
            if (open.ShowDialog() != DialogResult.OK) return;

            NAudio.Wave.WaveChannel32 wave = new NAudio.Wave.WaveChannel32(new NAudio.Wave.WaveFileReader(open.FileName));

                        byte[] buffer = new byte[16384];
                        int read = 0;

                        while (wave.Position < wave.Length)
                        {
                            read = wave.Read(buffer, 0, 16384);

                            for (int i = 0; i < read / 4; i++)
                            {
                                chart1.Series["wave"].Points.Add(BitConverter.ToSingle(buffer, i * 4));
                            }
                        } 

            using (WaveFileReader reader = new WaveFileReader(open.FileName))
            {
                IWaveProvider stream32 = new Wave16toFloatProvider(reader);
                IWaveProvider streamEffect = new AutoTuneWaveProvider(stream32, autotuneSettings);
                IWaveProvider stream16 = new WaveFloatTo16Provider(streamEffect);
                using (WaveFileWriter converted = new WaveFileWriter(tempFile, stream16.WaveFormat))
                {
                    // buffer length needs to be a power of 2 for FFT to work nicely
                    // however, make the buffer too long and pitches aren't detected fast enough
                    // successful buffer sizes: 8192, 4096, 2048, 1024
                    // (some pitch detection algorithms need at least 2048)
                    byte[] buffer = new byte[8192];
                    int bytesRead;
                    do
                    {
                        bytesRead = stream16.Read(buffer, 0, buffer.Length);
                        converted.WriteData(buffer, 0, bytesRead);
                    } while (bytesRead != 0 && converted.Length < reader.Length);
                }
            }
        }

      
    }
}
