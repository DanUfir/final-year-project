﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

[TestFixture]
public class UnitTest {
    GenerateObstacles obstacles;

	[Test]
	public void FirstTest() {      
        Assert.That(7 * 7 == 49);
	}
    [Test]
    public void SecondtTest()
    {
        Assert.That(7 * 7 == 51, "Not expected result");
    }
    
    [Test]
    public void TestStartingScore()
    {       
        Assert.True(GenerateObstacles.score == 0, "Starting Score must be 0");
    }

    [Test]
    public void TestStartingHighScore()
    {
        Assert.That(GenerateObstacles.highScore == 0, "Starting highscore must be 0");
    }


    [Test]
    public void TestScoreIncrease()
    {
        int score = GenerateObstacles.score;
        MoveObstacles mv = new MoveObstacles();

        mv.IncreaseScore();

        Assert.That(GenerateObstacles.score == score + 1, "Score Increasing");
        GenerateObstacles.score = 0;
    }
}
