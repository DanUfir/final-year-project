﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    AudioAnalysis mic = new AudioAnalysis();
    public Vector2 force = new Vector2(0, 200);
    public Vector2 curPos;
    public Rigidbody2D rigid;
	// Use this for initialization
	void Start () {
        rigid = GetComponent<Rigidbody2D>(); 
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(mic.AvgVolume());
		if(mic.AvgVolume() > .001f)
        {
            rigid.velocity = Vector2.zero;
            rigid.AddForce(force); 
        }
        curPos = Camera.main.WorldToScreenPoint(transform.position);
        
        if(curPos.y > Screen.height || curPos.y < 0)
        {
            Die();
        }
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        Die();
    }

    void Die()
    {        
        GenerateObstacles.score = 0;

        Application.LoadLevel(Application.loadedLevel);
    }
}
